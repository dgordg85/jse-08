package ru.kozyrev.tm;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.project.*;
import ru.kozyrev.tm.command.system.AboutCommand;
import ru.kozyrev.tm.command.system.ExitCommand;
import ru.kozyrev.tm.command.system.HelpCommand;
import ru.kozyrev.tm.command.task.*;
import ru.kozyrev.tm.command.user.*;
import ru.kozyrev.tm.context.Bootstrap;

public final class Application {
    private static final Class<?>[] CLASSES = {
            ProjectClearCommand.class, ProjectCreateCommand.class,
            ProjectListCommand.class, ProjectRemoveCommand.class,
            ProjectUpdateCommand.class,

            AboutCommand.class, ExitCommand.class, HelpCommand.class,

            TaskClearCommand.class, TaskCreateCommand.class,
            TaskListCommand.class, TaskRemoveCommand.class,
            TaskUpdateCommand.class,

            UserCreateCommand.class, UserLoginCommand.class,
            UserLogoutCommand.class, UserPasswordUpdateCommand.class,
            UserProfileCommand.class, UserRemoveCommand.class,
            UserUpdateCommand.class
    };

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
    }
}
