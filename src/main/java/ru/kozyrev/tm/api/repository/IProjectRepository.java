package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {
    @Nullable
    Project findOne(@NotNull final String id);

    @NotNull
    Project persist(@NotNull final Project entity) throws Exception;

    @NotNull
    Project merge(@NotNull final Project entity);

    @Nullable
    Project remove(@NotNull final String id);

    void removeAll();

    @Nullable
    List<Project> findAll();

    @Nullable
    List<Project> findAll(@NotNull final String userId);

    @Nullable
    Project findOne(@NotNull final String id, @NotNull final String userId);

    @Nullable
    Project merge(@NotNull final Project entity, @NotNull final String userId);

    @Nullable
    Project remove(@NotNull final String id, @NotNull final String userId);

    void removeAll(@NotNull final String userId);

    @Nullable
    Project findOne(@NotNull final Integer shortLink, @NotNull final String userId) throws Exception;
}
