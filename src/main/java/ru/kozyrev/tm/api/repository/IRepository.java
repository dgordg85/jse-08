package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<T> {
    @Nullable
    T findOne(@NotNull final String id);

    @NotNull
    T persist(@NotNull final T entity) throws Exception;

    @NotNull
    T merge(@NotNull final T entity);

    @Nullable
    T remove(@NotNull final String id);

    void removeAll();

    @Nullable
    List<T> findAll();

    @Nullable
    List<T> findAll(@NotNull final String userId);

    @Nullable
    T findOne(@NotNull final String id, @NotNull final String userId);

    @Nullable
    T merge(@NotNull final T entity, @NotNull final String userId);

    @Nullable
    T remove(@NotNull final String id, @NotNull final String userId);

    void removeAll(@NotNull final String userId);

    @Nullable
    T findOne(@NotNull final Integer shortLink, @NotNull final String userId) throws Exception;
}
