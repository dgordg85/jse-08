package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {
    @Nullable
    Task findOne(@NotNull final String id);

    @NotNull
    Task persist(@NotNull final Task entity) throws Exception;

    @NotNull
    Task merge(@NotNull final Task entity);

    @Nullable
    Task remove(@NotNull final String id);

    void removeAll();

    @Nullable
    List<Task> findAll();

    @Nullable
    List<Task> findAll(@NotNull final String userId);

    @Nullable
    Task findOne(@NotNull final String id, @NotNull final String userId);

    @Nullable
    Task merge(@NotNull final Task entity, @NotNull final String userId);

    @Nullable
    Task remove(@NotNull final String id, @NotNull final String userId);

    void removeAll(@NotNull final String userId);

    @Nullable
    Task findOne(@NotNull final Integer shortLink, @NotNull final String userId) throws Exception;

    @Nullable
    List<Task> findAll(@NotNull final String projectId, @NotNull final String userId);

    boolean removeProjectTasks(@NotNull final String projectId, @NotNull final String userId);
}
