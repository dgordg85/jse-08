package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.User;

import java.util.List;

public interface IUserRepository {
    @Nullable
    User findOne(@NotNull final String id);

    @NotNull
    User persist(@NotNull final User entity) throws Exception;

    @NotNull
    User merge(@NotNull final User entity);

    @Nullable
    User remove(@NotNull final String id);

    void removeAll();

    @Nullable
    List<User> findAll();

    @Nullable
    List<User> findAll(@NotNull final String userId);

    @Nullable
    User findOne(@NotNull final String id, @NotNull final String userId);

    @Nullable
    User merge(@NotNull final User entity, @NotNull final String userId);

    @Nullable
    User remove(@NotNull final String id, @NotNull final String userId);

    void removeAll(@NotNull final String userId);

    @Nullable
    User findOne(@NotNull final Integer shortLink, @NotNull final String userId) throws Exception;

    @Nullable
    User getUserByLogin(@NotNull final String login);
}
