package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Project;

import java.util.List;

public interface IProjectService {
    @Nullable
    List<Project> findAll();

    @Nullable
    Project findOne(@Nullable final String id, @Nullable final String userId) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable final String userId) throws Exception;

    @Nullable
    Project remove(@Nullable final String entityId, @Nullable final String userId) throws Exception;

    void removeAll(@Nullable final String userId) throws Exception;

    @NotNull
    String getEntityIdByNum(@Nullable final String num, @Nullable final String userId) throws Exception;

    @NotNull
    Project persist(@Nullable final Project project, @Nullable final String userId) throws Exception;

    @Nullable
    Project merge(@Nullable final Project project, @Nullable final String userId) throws Exception;
}
