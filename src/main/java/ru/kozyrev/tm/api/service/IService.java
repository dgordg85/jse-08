package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {
    @Nullable
    List<T> findAll();

    @Nullable
    T findOne(@Nullable final String id, @Nullable final String userId) throws Exception;

    @Nullable
    List<T> findAll(@Nullable final String userId) throws Exception;

    @Nullable
    T remove(@Nullable final String entityId, @Nullable final String userId) throws Exception;

    void removeAll(@Nullable final String userId) throws Exception;

    @NotNull
    String getEntityIdByNum(@Nullable final String num, @Nullable final String userId) throws Exception;
}
