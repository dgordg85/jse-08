package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Task;

import java.util.List;

public interface ITaskService {
    @Nullable
    List<Task> findAll();

    @Nullable
    Task findOne(@Nullable final String id, @Nullable final String userId) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable final String userId) throws Exception;

    @Nullable
    Task remove(@Nullable final String entityId, @Nullable final String userId) throws Exception;

    void removeAll(@Nullable final String userId) throws Exception;

    @NotNull
    String getEntityIdByNum(@Nullable final String num, @Nullable final String userId) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable final String projectId, @Nullable final String userId) throws Exception;

    @NotNull
    Task persist(@Nullable final Task task, @Nullable final String userId) throws Exception;

    @Nullable
    Task merge(@Nullable final Task task, @Nullable final String userId) throws Exception;

    boolean removeProjectTasks(@Nullable final String projectId, @Nullable final String userId) throws Exception;
}
