package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.command.CommandCorruptException;
import ru.kozyrev.tm.exception.command.CommandException;

import java.util.Scanner;

public interface ITerminalService {
    void printCommands();

    void registryCommand(@Nullable final AbstractCommand command) throws CommandCorruptException;

    void execute(@Nullable final String commandStr) throws Exception;

    boolean isCommandNotAllow(@Nullable final AbstractCommand command) throws CommandException;

    void initCommands(@NotNull Class<?>[] classes) throws Exception;

    void welcomeMsg();

    void errorDateMsg();

    @NotNull
    String nextLine();

    void setSc(@NotNull final Scanner sc);

    void closeSc();

    boolean isUserAuth();

    @Nullable
    User getCurrentUser();

    void setCurrentUser(@Nullable final User user);

    @Nullable
    String getCurrentUserId();
}
