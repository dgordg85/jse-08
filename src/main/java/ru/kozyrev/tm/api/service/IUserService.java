package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.User;

import java.util.List;

public interface IUserService {
    @Nullable
    List<User> findAll();

    @Nullable
    User findOne(@Nullable final String id, @Nullable final String userId) throws Exception;

    @Nullable
    List<User> findAll(@Nullable final String userId) throws Exception;

    @Nullable
    User remove(@Nullable final String entityId, @Nullable final String userId) throws Exception;

    void removeAll(@Nullable final String userId) throws Exception;

    @NotNull
    String getEntityIdByNum(@Nullable final String num, @Nullable final String userId) throws Exception;

    @Nullable
    User persist(@Nullable final User user) throws Exception;

    @Nullable
    User merge(@Nullable final User user) throws Exception;

    @Nullable
    User remove(@Nullable final String id);

    @Nullable
    User getUserByLogin(@Nullable final String login);

    boolean isPasswordTrue(@Nullable final String userId, @Nullable final String password) throws Exception;
}
