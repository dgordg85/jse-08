package ru.kozyrev.tm.api.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IProjects {
    @Nullable
    String getName();

    @Nullable
    String getDescription();

    @NotNull
    String getDateStartAsStr();

    @NotNull
    String getDateFinishAsStr();

    @Nullable
    Integer getShortLink();
}
