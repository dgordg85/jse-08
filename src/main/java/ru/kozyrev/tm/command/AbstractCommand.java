package ru.kozyrev.tm.command;

import lombok.Getter;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.ITerminalService;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.enumerated.RoleType;

import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class AbstractCommand {
    @NonNull
    private final List<RoleType> roleTypes = new ArrayList<>();

    @Nullable
    protected ServiceLocator serviceLocator;

    @Nullable
    protected ITerminalService terminalService;

    @NotNull
    protected Boolean secure = false;

    public AbstractCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    public final void setServiceLocator(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.terminalService = serviceLocator.getTerminalService();
    }

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getDescription();

    public abstract void execute() throws Exception;
}
