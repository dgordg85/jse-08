package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.util.TerminalUtil;

public final class ProjectClearCommand extends AbstractCommand {
    @NotNull
    @Override
    public final String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        @Nullable final String userId = terminalService.getCurrentUserId();

        System.out.println("[CLEAR]");
        System.out.println("Press 'ENTER' FOR ALL OR PROJECT ID]");
        @NotNull final String projectNum = terminalService.nextLine();
        if (projectNum.isEmpty()) {
            clearAll();
        } else {
            @Nullable final String projectId = serviceLocator.getProjectService().getEntityIdByNum(projectNum, userId);
            TerminalUtil.clearProject(
                    serviceLocator.getProjectService(),
                    serviceLocator.getTaskService(),
                    projectId,
                    terminalService.getCurrentUserId()
            );
        }
    }

    public final void clearAll() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        @Nullable final String userId = terminalService.getCurrentUserId();

        TerminalUtil.clearAllTasks(serviceLocator.getTaskService(), userId);
        serviceLocator.getProjectService().removeAll(userId);
        System.out.println("[ALL PROJECTS REMOVED]");
    }
}
