package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.util.DateUtil;
import ru.kozyrev.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public final String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Create new project.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        @Nullable final User currentUser = terminalService.getCurrentUser();
        if (currentUser == null) {
            throw new UserEmptyException();
        }

        @NotNull Project project = new Project();
        System.out.println("[PROJECT CREATE]\nENTER NAME:");
        project.setName(terminalService.nextLine());

        System.out.println("ENTER DESCRIPTION:");
        project.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATESTART:");
        project.setDateStart(DateUtil.parseDate(terminalService.nextLine()));

        System.out.println("ENTER DATEFINISH:");
        project.setDateFinish(DateUtil.parseDate(terminalService.nextLine()));

        project.setUserId(terminalService.getCurrentUser().getId());

        serviceLocator.getProjectService().persist(project, currentUser.getId());

        @Nullable List<Project> projectList = serviceLocator.getProjectService().findAll(currentUser.getId());
        TerminalUtil.printProjects(projectList);
    }
}
