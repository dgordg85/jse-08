package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @NotNull
    @Override
    public final String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show all projects.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        @Nullable final String userId = terminalService.getCurrentUserId();

        System.out.println("Press 'ENTER' for printing all projects list or input 'ID PROJECT'");
        @NotNull final String projectNum = terminalService.nextLine();

        if (projectNum.isEmpty()) {
            @Nullable List<Project> projectList = serviceLocator.getProjectService().findAll(userId);
            TerminalUtil.printProjects(projectList);
        } else {
            @Nullable String projectId = serviceLocator.getProjectService().getEntityIdByNum(projectNum, userId);
            @Nullable List<Task> taskList = serviceLocator.getTaskService().findAll(projectId, userId);
            TerminalUtil.printTasksOfProject(taskList);
        }
    }
}
