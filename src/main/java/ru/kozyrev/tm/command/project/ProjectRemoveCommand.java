package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.util.TerminalUtil;

public final class ProjectRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public final String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Remove Selected project.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        @Nullable final String userId = terminalService.getCurrentUserId();

        System.out.println("[PROJECT REMOVE]\nENTER ID:");
        @NotNull final String projectNum = terminalService.nextLine();

        @Nullable final String projectId = serviceLocator.getProjectService().getEntityIdByNum(projectNum, userId);
        TerminalUtil.clearProject(
                serviceLocator.getProjectService(),
                serviceLocator.getTaskService(),
                projectId,
                terminalService.getCurrentUserId()
        );
    }
}
