package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.util.DateUtil;

public final class ProjectUpdateCommand extends AbstractCommand {
    @NotNull
    @Override
    public final String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Update Selected project.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        @Nullable final String userId = terminalService.getCurrentUserId();
        if (userId == null) {
            throw new UserEmptyException();
        }
        @NotNull Project project = new Project();

        System.out.println("[UPDATE PROJECT]\nENTER ID:");
        project.setId(serviceLocator.getProjectService().getEntityIdByNum(terminalService.nextLine(), userId));

        System.out.println("ENTER NAME:");
        project.setName(terminalService.nextLine());

        System.out.println("ENTER DESCRIPTION:");
        project.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATESTART:");
        project.setDateStart(DateUtil.parseDate(terminalService.nextLine()));

        System.out.println("ENTER DATEFINISH:");

        project.setDateFinish(DateUtil.parseDate(terminalService.nextLine()));

        project.setUserId(userId);

        serviceLocator.getProjectService().merge(project, userId);
        System.out.println("[OK]");
    }
}
