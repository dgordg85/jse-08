package ru.kozyrev.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {
    public AboutCommand() {
        secure = true;
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print information about build";
    }

    @Override
    public final void execute() {
        @NotNull final String developer = Manifests.read("developer");
        @NotNull final String version = Manifests.read("version");
        System.out.printf("Developer: %s\nBuildNumber: %s\n", developer, version);
    }
}
