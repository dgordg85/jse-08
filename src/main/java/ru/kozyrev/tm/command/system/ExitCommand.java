package ru.kozyrev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.exception.command.ServiceFailException;

public final class ExitCommand extends AbstractCommand {
    public ExitCommand() {
        secure = true;
    }

    @NotNull
    @Override
    public final String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Quit from manager.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        terminalService.closeSc();
        System.exit(0);
    }
}
