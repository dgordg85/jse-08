package ru.kozyrev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.exception.command.ServiceFailException;

public final class HelpCommand extends AbstractCommand {
    public HelpCommand() {
        secure = true;
    }

    @NotNull
    @Override
    public final String getName() {
        return "help";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show all commands.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        System.out.println("[COMMANDS LIST]");
        System.out.println("DEFAULT USERS: user1:user1, admin:admin;");
        terminalService.printCommands();
    }
}
