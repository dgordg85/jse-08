package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.util.TerminalUtil;

public final class TaskClearCommand extends AbstractCommand {
    @NotNull
    @Override
    public final String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        @Nullable final String userId = terminalService.getCurrentUserId();
        System.out.println("[CLEAR]");

        System.out.println("Press 'ENTER' FOR ALL OR PROJECT ID]");
        @NotNull final String projectNum = terminalService.nextLine();

        if (projectNum.isEmpty()) {
            TerminalUtil.clearAllTasks(serviceLocator.getTaskService(), userId);
        } else {
            @Nullable final String projectId = serviceLocator.getProjectService().getEntityIdByNum(projectNum, userId);
            TerminalUtil.clearTasks(serviceLocator.getTaskService(), projectId, terminalService.getCurrentUserId());
        }
    }
}
