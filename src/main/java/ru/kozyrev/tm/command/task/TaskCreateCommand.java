package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.util.DateUtil;

public final class TaskCreateCommand extends AbstractCommand {
    public TaskCreateCommand() {
        this.secure = false;
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Create new tasks.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        @Nullable final String userId = terminalService.getCurrentUserId();
        if (userId == null) {
            throw new UserEmptyException();
        }

        @NotNull final Task task = new Task();

        System.out.println("[TASK CREATE]\nENTER NAME:");
        task.setName(terminalService.nextLine());

        System.out.println("ENTER PROJECT ID:");
        task.setProjectId(serviceLocator.getProjectService().getEntityIdByNum(terminalService.nextLine(), userId));

        System.out.println("ENTER DESCRIPTION:");
        task.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATESTART:");
        task.setDateStart(DateUtil.parseDate(terminalService.nextLine()));

        System.out.println("ENTER DATEFINISH:");

        task.setDateFinish(DateUtil.parseDate(terminalService.nextLine()));

        task.setUserId(userId);

        serviceLocator.getTaskService().persist(task, userId);
        System.out.println("[OK]");
    }
}
