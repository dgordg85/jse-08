package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {
    @NotNull
    @Override
    public final String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        @Nullable final String userId = terminalService.getCurrentUserId();

        System.out.println("PRESS 'ENTER' FOR ALL OR INPUT ID PROJECT");
        @NotNull final String projectNum = terminalService.nextLine();


        if (projectNum.isEmpty()) {
            TerminalUtil.printAllTasks(serviceLocator.getTaskService().findAll(terminalService.getCurrentUserId()));
        } else {
            @Nullable final String projectId = serviceLocator.getProjectService().getEntityIdByNum(projectNum, userId);
            @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAll(projectId, userId);
            TerminalUtil.printTasksOfProject(taskList);
        }
    }
}
