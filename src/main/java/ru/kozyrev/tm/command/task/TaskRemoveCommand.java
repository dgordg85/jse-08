package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.exception.command.ServiceFailException;

public final class TaskRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public final String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        @Nullable final String userId = terminalService.getCurrentUserId();

        System.out.println("[TASK DELETE]\nENTER ID:");
        @NotNull final String taskNum = terminalService.nextLine();

        @Nullable final String taskId = serviceLocator.getTaskService().getEntityIdByNum(taskNum, userId);
        serviceLocator.getTaskService().remove(taskId, userId);
        System.out.println("[OK]");
    }
}
