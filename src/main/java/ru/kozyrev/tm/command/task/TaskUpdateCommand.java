package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.util.DateUtil;

public final class TaskUpdateCommand extends AbstractCommand {
    @NotNull
    @Override
    public final String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Update selected task.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        @Nullable final String userId = terminalService.getCurrentUserId();
        if (userId == null) {
            throw new UserEmptyException();
        }

        @NotNull final Task task = new Task();

        System.out.println("[UPDATE TASK]\nENTER ID:");
        @NotNull final String num = terminalService.nextLine();
        @Nullable final String id = serviceLocator.getTaskService().getEntityIdByNum(num, userId);

        task.setId(id);

        System.out.println("NEW NAME:");
        task.setName(terminalService.nextLine());

        System.out.println("NEW PROJECT ID:");
        @NotNull String projectNum = terminalService.nextLine();

        if (!projectNum.isEmpty()) {
            @Nullable final String projectId = serviceLocator.getProjectService().getEntityIdByNum(projectNum, userId);
            task.setProjectId(projectId);
        } else {
            task.setProjectId("");
        }

        System.out.println("ENTER DESCRIPTION:");
        task.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATESTART:");
        @NotNull final String dateBegin = terminalService.nextLine();

        if (!dateBegin.isEmpty()) {
            task.setDateStart(DateUtil.parseDate(dateBegin));
        } else {
            task.setDateStart(null);
        }

        System.out.println("ENTER DATEFINISH:");
        @NotNull final String dateFinish = terminalService.nextLine();

        if (!dateFinish.isEmpty()) {
            task.setDateFinish(DateUtil.parseDate(dateFinish));
        } else {
            task.setDateFinish(null);
        }

        task.setUserId(userId);

        serviceLocator.getTaskService().merge(task, userId);
        System.out.println("[OK]");
    }
}
