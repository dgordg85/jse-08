package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.user.UserPasswordMatchException;
import ru.kozyrev.tm.util.HashUtil;

public final class UserCreateCommand extends AbstractCommand {
    public UserCreateCommand() {
        secure = true;
    }

    @Override
    public final @NotNull String getName() {
        return "user-create";
    }

    @Override
    public final @NotNull String getDescription() {
        return "User for registry new user";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }

        @NotNull final User user = new User();

        System.out.println("[USER REGISTRATION]");

        System.out.println("ENTER LOGIN:");
        user.setLogin(terminalService.nextLine());

        System.out.println("ENTER PASSWORD:");
        final String password = terminalService.nextLine();

        System.out.println("RE-ENTER PASSWORD:");
        final String password2 = terminalService.nextLine();

        if (!password.equals(password2)) {
            throw new UserPasswordMatchException();
        }
        user.setPasswordHash(HashUtil.getHash(password));
        if (serviceLocator.getUserService().persist(user) == null) {
            throw new RuntimeException();
        }
        System.out.println("[OK]");
    }
}
