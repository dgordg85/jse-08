package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.user.UserLoginNotRegistryException;
import ru.kozyrev.tm.exception.user.UserPasswordWrongException;

public final class UserLoginCommand extends AbstractCommand {
    public UserLoginCommand() {
        secure = true;
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Use for logging in system.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }

        System.out.println("[LOGIN]");

        System.out.println("ENTER LOGIN:");
        @NotNull final String login = terminalService.nextLine();

        @Nullable final User user = serviceLocator.getUserService().getUserByLogin(login);
        if (user == null) {
            throw new UserLoginNotRegistryException();
        }
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = terminalService.nextLine();

        final boolean isPasswordTrue = serviceLocator.getUserService().isPasswordTrue(user.getId(), password);
        if (!isPasswordTrue) {
            throw new UserPasswordWrongException();
        }
        terminalService.setCurrentUser(user);
        System.out.println("[User just login!]");
    }
}
