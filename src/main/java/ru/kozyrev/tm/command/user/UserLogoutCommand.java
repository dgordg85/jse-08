package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.exception.command.ServiceFailException;

public final class UserLogoutCommand extends AbstractCommand {
    @NotNull
    @Override
    public final String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Use for logout.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }

        terminalService.setCurrentUser(null);
        System.out.println("[User logout!]");
    }
}
