package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.user.*;
import ru.kozyrev.tm.util.HashUtil;

public final class UserPasswordUpdateCommand extends AbstractCommand {
    @NotNull
    @Override
    public final String getName() {
        return "user-pass-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Use for updating password";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }

        System.out.println("[UPDATE PASSWORD]");

        System.out.println("ENTER CURRENT PASSWORD:");
        @NotNull final String password = terminalService.nextLine();

        @Nullable final User currentUser = terminalService.getCurrentUser();
        if (currentUser == null) {
            throw new UserEmptyException();
        }
        if (!serviceLocator.getUserService().isPasswordTrue(currentUser.getId(), password)) {
            throw new UserPasswordWrongException();
        }

        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String newPassword = terminalService.nextLine();

        System.out.println("RE-ENTER PASSWORD:");
        @NotNull final String reNewPassword = terminalService.nextLine();

        if (!newPassword.equals(reNewPassword)) {
            throw new UserPasswordMatchException();
        }
        @NotNull final User user = new User();
        user.setId(currentUser.getId());
        @Nullable final String currentUserLogin = currentUser.getLogin();
        if (currentUserLogin == null) {
            throw new UserLoginEmptyException();
        }
        user.setLogin(currentUserLogin);
        user.setPasswordHash(HashUtil.getHash(reNewPassword));
        user.setRoleType(terminalService.getCurrentUser().getRoleType());
        final User mergeUser = serviceLocator.getUserService().merge(user);
        if (mergeUser == null) {
            throw new UserPasswordChangeException();
        }
        terminalService.setCurrentUser(user);
        System.out.println("[PASSWORD UPDATE]");
    }
}
