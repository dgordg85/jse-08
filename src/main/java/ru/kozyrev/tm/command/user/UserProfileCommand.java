package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.user.UserEmptyException;

public final class UserProfileCommand extends AbstractCommand {
    @NotNull
    @Override
    public final String getName() {
        return "user-profile";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show user profile";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        User currentUser = terminalService.getCurrentUser();
        if (currentUser == null) {
            throw new UserEmptyException();
        }
        System.out.println("[USER PROFILE]");
        System.out.printf("Login: %s\n", currentUser.getLogin());
        System.out.printf("Role: %s\n", currentUser.getRoleType().getDisplayName());
    }
}
