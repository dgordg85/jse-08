package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.user.UserEmptyException;

public final class UserRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-remove";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Use for deleting profile";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        @Nullable final User currentUser = terminalService.getCurrentUser();
        if (currentUser == null) {
            throw new UserEmptyException();
        }

        System.out.println("[USER REMOVE]");
        System.out.println("Are you sure? Type 'yes' or another for cancel...");
        @NotNull final String answer = terminalService.nextLine();
        if ("yes".equals(answer.toLowerCase())) {
            @Nullable final String userId = currentUser.getId();
            serviceLocator.getUserService().remove(userId);
            System.out.println("[USER DELETE!]");
            terminalService.setCurrentUser(null);
        } else {
            System.out.println("[OPERATION ABORT!]");
        }
    }
}
