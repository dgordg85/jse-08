package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.user.*;

public final class UserUpdateCommand extends AbstractCommand {
    @NotNull
    @Override
    public final String getName() {
        return "user-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Update user login.";
    }

    @Override
    public final void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) {
            throw new ServiceFailException();
        }
        @Nullable final User currentUser = terminalService.getCurrentUser();
        if (currentUser == null) {
            throw new UserEmptyException();
        }

        System.out.println("[UPDATE USER]");

        System.out.println("ENTER NEW LOGIN:");
        @NotNull String login = terminalService.nextLine();

        if (login.isEmpty()) {
            throw new UserLoginEmptyException();
        }
        if (serviceLocator.getUserService().getUserByLogin(login) != null) {
            throw new UserLoginTakenException();
        }

        @Nullable final User user = new User();
        user.setId(currentUser.getId());
        user.setLogin(login);

        String password = currentUser.getPasswordHash();
        if (password == null) {
            throw new UserPasswordEmptyException();
        }
        user.setPasswordHash(password);

        user.setRoleType(terminalService.getCurrentUser().getRoleType());
        final User mergeUser = serviceLocator.getUserService().merge(user);
        if (mergeUser == null) {
            throw new UserLoginChangeFailException();
        }
        terminalService.setCurrentUser(user);
        System.out.println("[LOGIN UPDATE]");
    }
}
