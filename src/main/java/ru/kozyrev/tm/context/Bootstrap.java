package ru.kozyrev.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.*;
import ru.kozyrev.tm.exception.entity.DateException;
import ru.kozyrev.tm.repository.ProjectRepository;
import ru.kozyrev.tm.repository.TaskRepository;
import ru.kozyrev.tm.repository.UserRepository;
import ru.kozyrev.tm.service.ProjectService;
import ru.kozyrev.tm.service.TaskService;
import ru.kozyrev.tm.service.TerminalService;
import ru.kozyrev.tm.service.UserService;
import ru.kozyrev.tm.util.EntityUtil;

import java.text.ParseException;

@Getter
public final class Bootstrap implements ServiceLocator {
    @NotNull
    private final IProjectService projectService = new ProjectService(new ProjectRepository());

    @NotNull
    private final ITaskService taskService = new TaskService(new TaskRepository());

    @NotNull
    private final IUserService userService = new UserService(new UserRepository());

    @NotNull
    private final ITerminalService terminalService = new TerminalService(this);

    public final void init(Class<?>[] classes) throws Exception {
        getTerminalService().initCommands(classes);
        EntityUtil.createDefaultUsers(getUserService());
        getTerminalService().welcomeMsg();
        @Nullable String command;
        while (true) {
            try {
                command = terminalService.nextLine().toLowerCase().replace('_', '-');
                getTerminalService().execute(command);
            } catch (ParseException | DateException e) {
                getTerminalService().errorDateMsg();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
