package ru.kozyrev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@Setter
@Getter
public abstract class AbstractEntity {
    @Nullable
    public String userId;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private Integer shortLink;
}


