package ru.kozyrev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.util.IProjects;
import ru.kozyrev.tm.util.DateUtil;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractEntity implements IProjects {
    @Nullable
    private String name;

    @Nullable
    private String projectId;

    @Nullable
    private String description;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    public Task(@NotNull String name,
                @NotNull String projectId,
                @NotNull String description,
                @NotNull String dateStart,
                @NotNull String dateFinish,
                @NotNull String userId
    ) throws Exception {
        this.name = name;
        this.projectId = projectId;
        this.description = description;
        this.dateStart = DateUtil.parseDate(dateStart);
        this.dateFinish = DateUtil.parseDate(dateFinish);
        this.userId = userId;
    }

    @NotNull
    @Override
    public final String getDateStartAsStr() {
        if (dateStart == null) {
            return "";
        }
        return DateUtil.getDate(dateStart);
    }

    @NotNull
    @Override
    public final String getDateFinishAsStr() {
        if (dateFinish == null) {
            return "";
        }
        return DateUtil.getDate(dateFinish);
    }
}
