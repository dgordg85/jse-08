package ru.kozyrev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.enumerated.RoleType;

@Getter
@Setter
public final class User extends AbstractEntity {
    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @NotNull
    private RoleType roleType = RoleType.USER;
}
