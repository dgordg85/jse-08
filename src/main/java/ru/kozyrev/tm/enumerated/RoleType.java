package ru.kozyrev.tm.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Getter
@RequiredArgsConstructor
public enum RoleType {
    ADMIN("Администратор"),
    USER("Обычный пользователь");

    @NotNull
    private final String displayName;
}
