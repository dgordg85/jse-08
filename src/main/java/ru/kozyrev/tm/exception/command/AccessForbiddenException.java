package ru.kozyrev.tm.exception.command;

public final class AccessForbiddenException extends Exception {
    public AccessForbiddenException() {
        super("Access forbidden for this command!");
    }
}
