package ru.kozyrev.tm.exception.command;

public class ServiceFailException extends Exception {
    public ServiceFailException() {
        super("TerminalService or ServiceLocator is not registry!");
    }
}
