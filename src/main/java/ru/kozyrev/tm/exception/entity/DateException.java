package ru.kozyrev.tm.exception.entity;

public final class DateException extends Exception {
    public DateException() {
        super("Wrong date! Use dd-MM-YYYY format!");
    }
}
