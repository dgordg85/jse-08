package ru.kozyrev.tm.exception.entity;

public final class EntityOwnerException extends Exception {
    public EntityOwnerException() {
        super("Owner is empty!");
    }
}
