package ru.kozyrev.tm.exception.entity;

public final class IndexException extends Exception {
    public IndexException() {
        super("Wrong index!");
    }
}
