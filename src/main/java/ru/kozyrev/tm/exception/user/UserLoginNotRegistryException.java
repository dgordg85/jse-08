package ru.kozyrev.tm.exception.user;

public final class UserLoginNotRegistryException extends Exception {
    public UserLoginNotRegistryException() {
        super("User not registry!");
    }
}
