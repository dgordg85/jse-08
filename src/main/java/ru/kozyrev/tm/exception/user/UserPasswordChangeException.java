package ru.kozyrev.tm.exception.user;

public final class UserPasswordChangeException extends Exception {
    public UserPasswordChangeException() {
        super("Password not update");
    }
}
