package ru.kozyrev.tm.exception.user;

public final class UserPasswordWrongException extends Exception {
    public UserPasswordWrongException() {
        super("Wrong password!");
    }
}
