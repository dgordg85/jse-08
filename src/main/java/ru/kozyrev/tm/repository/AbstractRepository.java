package ru.kozyrev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IRepository;
import ru.kozyrev.tm.entity.AbstractEntity;
import ru.kozyrev.tm.exception.entity.EntityException;
import ru.kozyrev.tm.exception.entity.IndexException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {
    private int count = 1;

    @NotNull
    private final Map<String, T> map = new LinkedHashMap<>();

    @Nullable
    @Override
    public final T findOne(@NotNull final String id) {
        return map.get(id);
    }

    @NotNull
    @Override
    public final T persist(@NotNull final T entity) throws Exception {
        if (map.containsKey(entity.getId())) {
            throw new EntityException();
        }
        entity.setShortLink(count++);
        map.put(entity.getId(), entity);
        return entity;
    }

    @NotNull
    @Override
    public final T merge(@NotNull final T entity) {
        map.put(entity.getId(), entity);
        return entity;
    }

    @Nullable
    @Override
    public final T remove(@NotNull final String id) {
        return map.remove(id);
    }

    @Override
    public final void removeAll() {
        map.clear();
    }

    @Nullable
    @Override
    public final List<T> findAll() {
        if (map.size() == 0) {
            return null;
        }
        return new ArrayList<>(map.values());
    }

    @Nullable
    @Override
    public final List<T> findAll(@NotNull final String userId) {
        @NotNull final List<T> list = new ArrayList<>();

        @Nullable final List<T> allEntities = findAll();
        if (allEntities == null) {
            return null;
        }
        for (T entity : allEntities) {
            if (entity.getUserId() == null) {
                continue;
            }
            if (entity.getUserId().equals(userId)) {
                list.add(entity);
            }
        }
        if (list.size() == 0) {
            return null;
        }
        return list;
    }

    @Nullable
    @Override
    public final T findOne(@NotNull final String id, @NotNull final String userId) {
        @Nullable final T entity = findOne(id);
        if (entity == null || entity.getUserId() == null) {
            return null;
        }
        return entity.getUserId().equals(userId) ? entity : null;
    }

    @Nullable
    @Override
    public final T merge(
            @NotNull final T entity,
            @NotNull final String userId
    ) {
        @Nullable final T entityUpdate = findOne(entity.getId());
        if (entityUpdate == null || entityUpdate.getUserId() == null) {
            return null;
        }
        if (entityUpdate.getUserId().equals(userId)) {
            merge(entity);
            return entity;
        }
        return null;
    }

    @Nullable
    @Override
    public final T remove(@NotNull final String id, @NotNull final String userId) {
        @Nullable final T entity = findOne(id);
        if (entity == null || entity.getUserId() == null) {
            return null;
        }
        if (entity.getUserId().equals(userId)) {
            return remove(id);
        }
        return null;
    }

    @Override
    public final void removeAll(@NotNull final String userId) {
        @Nullable final List<T> allEntities = findAll();

        if (allEntities != null) {
            for (T entity : allEntities) {
                if (entity == null || entity.getUserId() == null) {
                    continue;
                }
                if (entity.getUserId().equals(userId)) {
                    remove(entity.getId());
                }
            }
        }
    }

    @Nullable
    @Override
    public final T findOne(
            @NotNull final Integer shortLink,
            @NotNull final String userId
    ) throws Exception {
        @Nullable final List<T> allEntities = findAll();
        if (allEntities == null) {
            return null;
        }

        for (T entity : allEntities) {
            if (entity == null || entity.getShortLink() == null || entity.getUserId() == null) {
                continue;
            }
            if (entity.getShortLink().equals(shortLink)) {
                if (entity.getUserId().equals(userId)) {
                    return entity;
                } else {
                    throw new IndexException();
                }
            }
        }
        return null;
    }
}
