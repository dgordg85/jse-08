package ru.kozyrev.tm.repository;

import ru.kozyrev.tm.api.repository.IProjectRepository;
import ru.kozyrev.tm.entity.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {
}
