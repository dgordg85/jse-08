package ru.kozyrev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.ITaskRepository;
import ru.kozyrev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {
    @Nullable
    @Override
    public final List<Task> findAll(@NotNull final String projectId, @NotNull final String userId) {
        @Nullable final List<Task> taskList = findAll(userId);
        if (taskList == null) {
            return null;
        }

        @NotNull final List<Task> result = new ArrayList<>();
        @Nullable String currentProjectId;
        @Nullable String currentTaskUserId;
        for (Task task : taskList) {
            currentProjectId = task.getProjectId();
            currentTaskUserId = task.getUserId();
            if (currentProjectId == null || currentTaskUserId == null) {
                continue;
            }
            if (currentProjectId.equals(projectId) && currentTaskUserId.equals(userId)) {
                result.add(task);
            }
        }
        return result;
    }

    @Override
    public final boolean removeProjectTasks(@NotNull final String projectId, @NotNull final String userId) {
        boolean isDelete = false;

        @Nullable final List<Task> list = findAll(userId);
        if (list == null) {
            return false;
        }
        @Nullable String currentProjectId;
        for (Task task : list) {
            currentProjectId = task.getProjectId();
            if (currentProjectId == null) {
                continue;
            }
            if (currentProjectId.equals(projectId)) {
                remove(task.getId());
                isDelete = true;
            }
        }
        return isDelete;
    }
}