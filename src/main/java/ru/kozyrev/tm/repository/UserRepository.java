package ru.kozyrev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IUserRepository;
import ru.kozyrev.tm.entity.User;

import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
    @Nullable
    @Override
    public final User getUserByLogin(@NotNull final String login) {
        @Nullable final List<User> list = findAll();
        if (list == null) {
            return null;
        }

        @Nullable String userLogin;
        for (User user : list) {
            userLogin = user.getLogin();
            if (userLogin == null) {
                continue;
            }
            if (userLogin.equals(login)) {
                return user;
            }
        }
        return null;
    }
}
