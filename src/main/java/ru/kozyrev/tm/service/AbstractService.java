package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.IService;
import ru.kozyrev.tm.entity.AbstractEntity;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.repository.AbstractRepository;
import ru.kozyrev.tm.util.StringUtil;

import java.util.List;

@NoArgsConstructor
public class AbstractService<T extends AbstractEntity> implements IService<T> {
    @Nullable
    AbstractRepository<T> abstractRepository = null;

    public AbstractService(@Nullable AbstractRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Nullable
    @Override
    public final List<T> findAll() {
        if (abstractRepository == null) {
            throw new RuntimeException();
        }
        return abstractRepository.findAll();
    }

    @Nullable
    @Override
    public final T findOne(
            @Nullable final String id,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (id == null || id.isEmpty()) {
            throw new IndexException();
        }
        if (abstractRepository == null) {
            throw new RuntimeException();
        }
        return abstractRepository.findOne(id);
    }

    @Nullable
    @Override
    public final List<T> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (abstractRepository == null) {
            throw new RuntimeException();
        }
        return abstractRepository.findAll(userId);
    }

    @Nullable
    @Override
    public final T remove(@Nullable final String entityId, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (entityId == null || entityId.isEmpty()) {
            throw new IndexException();
        }
        if (abstractRepository == null) {
            throw new RuntimeException();
        }
        return abstractRepository.remove(entityId, userId);
    }

    @Override
    public final void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (abstractRepository == null) {
            throw new RuntimeException();
        }
        abstractRepository.removeAll(userId);
    }

    @NotNull
    @Override
    public final String getEntityIdByNum(
            @Nullable final String num,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (num == null || num.isEmpty()) {
            throw new IndexException();
        }

        final int shortLink = StringUtil.parseToInt(num);
        if (abstractRepository == null) {
            throw new RuntimeException();
        }
        @Nullable final T entity = abstractRepository.findOne(shortLink, userId);
        if (entity == null) {
            throw new IndexException();
        }
        return entity.getId();
    }
}
