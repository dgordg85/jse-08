package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IProjectRepository;
import ru.kozyrev.tm.api.service.IProjectService;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.exception.entity.DateException;
import ru.kozyrev.tm.exception.entity.DescriptionException;
import ru.kozyrev.tm.exception.entity.EntityOwnerException;
import ru.kozyrev.tm.exception.entity.NameException;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.repository.ProjectRepository;

@NoArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {
    @Nullable
    private final IProjectRepository projectRepository = (ProjectRepository) abstractRepository;

    public ProjectService(@Nullable ProjectRepository projectRepository) {
        super(projectRepository);
    }

    @NotNull
    @Override
    public final Project persist(@Nullable final Project project, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (project == null) {
            throw new RuntimeException();
        }
        if (project.getName() == null || project.getName().isEmpty()) {
            throw new NameException();
        }
        if (project.getUserId() == null || project.getUserId().isEmpty()) {
            throw new EntityOwnerException();
        }
        if (project.getDescription() == null || project.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }
        if (projectRepository == null) {
            throw new RuntimeException();
        }
        return projectRepository.persist(project);
    }

    @Nullable
    @Override
    public final Project merge(@Nullable final Project project, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (project == null) {
            throw new RuntimeException();
        }
        if (project.getName() == null) {
            throw new NameException();
        }
        if (project.getUserId() == null || project.getUserId().isEmpty()) {
            throw new EntityOwnerException();
        }
        if (project.getDescription() == null) {
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }

        @Nullable final Project projectUpdate = findOne(project.getId(), userId);
        if (projectUpdate == null) {
            return persist(project, userId);
        }
        if (!project.getName().isEmpty()) {
            projectUpdate.setName(project.getName());
        }
        if (!project.getDescription().isEmpty()) {
            projectUpdate.setDescription(project.getDescription());
        }
        projectUpdate.setDateStart(project.getDateStart());
        projectUpdate.setDateFinish(project.getDateFinish());
        if (projectRepository == null) {
            throw new RuntimeException();
        }
        return projectRepository.merge(projectUpdate, userId);
    }
}
