package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.ITaskRepository;
import ru.kozyrev.tm.api.service.ITaskService;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.entity.*;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.repository.TaskRepository;

import java.util.List;

@NoArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {
    @Nullable
    private final ITaskRepository taskRepository = (TaskRepository) abstractRepository;

    public TaskService(@Nullable TaskRepository taskRepository) {
        super(taskRepository);
    }

    @Nullable
    @Override
    public final List<Task> findAll(@Nullable final String projectId, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        if (taskRepository == null) {
            throw new RuntimeException();
        }
        return taskRepository.findAll(projectId, userId);
    }

    @NotNull
    @Override
    public final Task persist(@Nullable final Task task, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (task == null) {
            throw new RuntimeException();
        }
        if (task.getName() == null || task.getName().isEmpty()) {
            throw new NameException();
        }
        if (task.getUserId() == null || task.getUserId().isEmpty()) {
            throw new EntityOwnerException();
        }
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
            throw new IndexException();
        }
        if (task.getDescription() == null || task.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (task.getDateStart() == null) {
            throw new DateException();
        }
        if (task.getDateFinish() == null) {
            throw new DateException();
        }
        if (taskRepository == null) {
            throw new RuntimeException();
        }
        return taskRepository.persist(task);
    }

    @Nullable
    @Override
    public final Task merge(@Nullable final Task task, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (task == null) {
            throw new RuntimeException();
        }
        if (task.getName() == null) {
            throw new NameException();
        }
        if (task.getUserId() == null || task.getUserId().isEmpty()) {
            throw new EntityOwnerException();
        }
        if (task.getProjectId() == null) {
            throw new IndexException();
        }
        if (task.getDescription() == null) {
            throw new DescriptionException();
        }

        @Nullable final Task taskUpdate = findOne(task.getId(), userId);
        if (taskUpdate == null) {
            return persist(task, userId);
        }

        if (!task.getName().isEmpty()) {
            taskUpdate.setName(task.getName());
        }
        if (!task.getProjectId().isEmpty()) {
            taskUpdate.setProjectId(task.getProjectId());
        }
        if (!task.getDescription().isEmpty()) {
            taskUpdate.setDescription(task.getDescription());
        }
        if (task.getDateStart() != null) {
            taskUpdate.setDateStart(task.getDateStart());
        }
        if (task.getDateFinish() != null) {
            taskUpdate.setDateFinish(task.getDateFinish());
        }
        if (taskRepository == null) {
            throw new RuntimeException();
        }
        return taskRepository.merge(taskUpdate, userId);
    }

    @Override
    public final boolean removeProjectTasks(@Nullable final String projectId, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        if (taskRepository == null) {
            throw new RuntimeException();
        }
        return taskRepository.removeProjectTasks(projectId, userId);
    }
}
