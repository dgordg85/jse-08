package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.ITerminalService;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.context.Bootstrap;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.command.AccessForbiddenException;
import ru.kozyrev.tm.exception.command.CommandCorruptException;
import ru.kozyrev.tm.exception.command.CommandException;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

@NoArgsConstructor
public class TerminalService implements ITerminalService {
    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Nullable
    private Bootstrap bootstrap = null;

    @NotNull
    private Scanner sc = new Scanner(System.in);

    @Nullable
    private User currentUser;

    public TerminalService(@Nullable Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public final void printCommands() {
        for (AbstractCommand command : commands.values()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }

    @Override
    public final void registryCommand(@Nullable final AbstractCommand command) throws CommandCorruptException {
        if (command == null) {
            throw new CommandCorruptException();
        }
        if (command.getName() == null || command.getName().isEmpty())
            throw new CommandCorruptException();
        if (command.getDescription() == null || command.getDescription().isEmpty())
            throw new CommandCorruptException();
        if (bootstrap == null) {
            throw new RuntimeException();
        }
        command.setServiceLocator(bootstrap);
        commands.put(command.getName(), command);
    }

    @Override
    public final void execute(@Nullable final String commandStr) throws Exception {
        if (commandStr == null || commandStr.isEmpty()) {
            throw new CommandException();
        }
        AbstractCommand command = commands.get(commandStr);
        if (isCommandNotAllow(command)) {
            throw new AccessForbiddenException();
        }
        command.execute();
    }

    @Override
    public final boolean isCommandNotAllow(@Nullable final AbstractCommand command) throws CommandException {
        if (command == null) {
            throw new CommandException();
        }
        final boolean isSecureCheck = command.getSecure() || isUserAuth();

        boolean isRoleAllowed;
        if (currentUser != null) {
            isRoleAllowed = command.getRoleTypes().contains(currentUser.getRoleType());
        } else
            isRoleAllowed = true;

        return !isSecureCheck || !isRoleAllowed;
    }

    @Override
    public final void initCommands(@NotNull Class<?>[] classes) throws Exception {
        for (@NotNull Class<?> clazz : classes) {
            registryCommand((AbstractCommand) clazz.getDeclaredConstructor().newInstance());
        }
    }

    @Override
    public final void welcomeMsg() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    @Override
    public final void errorDateMsg() {
        System.out.println("Wrong date! User dd-MM-YYYY format");
    }

    @NotNull
    @Override
    public final String nextLine() {
        return sc.nextLine();
    }

    @Override
    public final void setCurrentUser(@Nullable final User user) {
        currentUser = user;
    }

    @Override
    public final void closeSc() {
        sc.close();
    }

    @Override
    public final boolean isUserAuth() {
        return currentUser != null;
    }

    @Override
    public final void setSc(@NotNull final Scanner sc) {
        this.sc = sc;
    }

    @Nullable
    @Override
    public final User getCurrentUser() {
        return currentUser;
    }

    @Nullable
    @Override
    public final String getCurrentUserId() {
        if (currentUser == null) {
            return null;
        }
        return currentUser.getId();
    }
}
