package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IUserRepository;
import ru.kozyrev.tm.api.service.IUserService;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.exception.user.UserLoginEmptyException;
import ru.kozyrev.tm.exception.user.UserLoginTakenException;
import ru.kozyrev.tm.exception.user.UserPasswordEmptyException;
import ru.kozyrev.tm.repository.UserRepository;
import ru.kozyrev.tm.util.HashUtil;

@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {
    @Nullable
    private final IUserRepository userRepository = (UserRepository) abstractRepository;

    public UserService(@NotNull UserRepository userRepository) {
        super(userRepository);
    }

    @Nullable
    @Override
    public final User persist(@Nullable final User user) throws Exception {
        if (user == null) {
            return null;
        }
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            throw new UserLoginEmptyException();
        }
        if (getUserByLogin(user.getLogin()) != null) {
            throw new UserLoginTakenException();
        }
        @Nullable final String userPassword = user.getPasswordHash();
        if (userPassword == null || userPassword.isEmpty() || userPassword.equals(HashUtil.EMPTY_PASSWORD)) {
            throw new UserPasswordEmptyException();
        }
        if (userRepository == null) {
            throw new RuntimeException();
        }
        return userRepository.persist(user);
    }

    @Nullable
    @Override
    public final User merge(@Nullable final User user) throws Exception {
        if (user == null) {
            return null;
        }
        if (user.getLogin() == null) {
            throw new UserLoginEmptyException();
        }
        if (user.getPasswordHash() == null) {
            throw new UserPasswordEmptyException();
        }
        if (userRepository == null) {
            throw new RuntimeException();
        }
        @Nullable final User userUpdate = userRepository.findOne(user.getId());
        if (userUpdate == null) {
            return null;
        }

        if (!user.getLogin().isEmpty()) {
            userUpdate.setLogin(user.getLogin());
        }
        if (user.getPasswordHash().isEmpty() || user.getPasswordHash().equals(HashUtil.EMPTY_PASSWORD)) {
            throw new UserPasswordEmptyException();
        }
        userUpdate.setPasswordHash(user.getPasswordHash());
        userUpdate.setRoleType(user.getRoleType());

        return userRepository.merge(userUpdate);
    }

    @Nullable
    @Override
    public final User remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) {
            return null;
        }
        if (userRepository == null) {
            throw new RuntimeException();
        }
        return userRepository.remove(id);
    }

    @Nullable
    @Override
    public final User getUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) {
            return null;
        }
        if (userRepository == null) {
            throw new RuntimeException();
        }
        return userRepository.getUserByLogin(login);
    }

    @Override
    public final boolean isPasswordTrue(
            @Nullable final String userId,
            @Nullable final String password
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (password == null || password.isEmpty()) {
            throw new UserPasswordEmptyException();
        }
        if (userRepository == null) {
            throw new RuntimeException();
        }
        @Nullable final User user = userRepository.findOne(userId);
        if (user == null) {
            return false;
        }
        @Nullable final String userPassword = user.getPasswordHash();
        if (userPassword == null) {
            return false;
        }
        return userPassword.equals(HashUtil.getHash(password));
    }
}
