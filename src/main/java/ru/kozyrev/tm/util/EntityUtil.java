package ru.kozyrev.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.service.IUserService;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;

public final class EntityUtil {
    public final static void createDefaultUsers(@NotNull final IUserService userService) throws Exception {
        @NotNull User user = new User();
        user.setLogin("user1");
        user.setPasswordHash(HashUtil.getHash("user1"));
        user.setRoleType(RoleType.USER);
        userService.persist(user);

        @NotNull User admin = new User();
        admin.setLogin("admin");
        admin.setPasswordHash(HashUtil.getHash("admin"));
        admin.setRoleType(RoleType.ADMIN);
        userService.persist(admin);
    }
}
