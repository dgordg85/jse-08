package ru.kozyrev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.IProjectService;
import ru.kozyrev.tm.api.service.ITaskService;
import ru.kozyrev.tm.api.util.IProjects;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.entity.IndexException;

import java.util.List;

public final class TerminalUtil {
    public final static void printAllTasks(@Nullable final List<Task> taskList) {
        System.out.println("[TASKS LIST]");
        if (taskList != null) {
            printList(taskList);
        } else {
            System.out.println("[EMPTY]");
        }
    }

    public final static void printTasksOfProject(@Nullable final List<Task> taskList) {
        System.out.println("[TASKS LIST OF PROJECT]");
        if (taskList != null) {
            printList(taskList);
        } else {
            System.out.println("[EMPTY]");
        }
    }

    public final static void printProjects(@Nullable final List<Project> projectList) {
        System.out.println("[PROJECTS LIST]");
        if (projectList != null) {
            printList(projectList);
        } else {
            System.out.println("[EMPTY]");
        }
    }

    public final static <T extends IProjects> void printList(@Nullable final List<T> list) {
        int count = 1;
        if (list == null) {
            System.out.println("[EMPTY]");
            return;
        }
        for (T entity : list) {
            System.out.printf("%d. %s, EDIT ID#%d, %s, s:%s, f:%s\n",
                    count++,
                    entity.getName(),
                    entity.getShortLink(),
                    entity.getDescription(),
                    entity.getDateStartAsStr(),
                    entity.getDateFinishAsStr()
            );
        }
    }

    public final static void clearProject(
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService,
            @Nullable final String projectId,
            @Nullable final String userId
    ) throws Exception {
        clearTasks(taskService, projectId, userId);
        if (projectService.remove(projectId, userId) == null) {
            throw new IndexException();
        }
        System.out.println("[PROJECT REMOVED]");
        printProjects(projectService.findAll(userId));
    }

    public final static void clearTasks(
            @NotNull final ITaskService taskService,
            @Nullable final String projectId,
            @Nullable final String userId
    ) throws Exception {
        boolean isAnyTaskDelete = taskService.removeProjectTasks(projectId, userId);
        if (isAnyTaskDelete) {
            System.out.println("[ALL TASKS OF PROJECT REMOVED]");
        } else {
            System.out.println("[PROJECT HAVE NO TASKS]");
        }
    }

    public final static void clearAllTasks(
            @NotNull final ITaskService taskService,
            @Nullable final String userId
    ) throws Exception {
        taskService.removeAll(userId);
        System.out.println("[ALL TASKS REMOVED]");
    }
}
